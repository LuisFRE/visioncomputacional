#Revisado por: Luis Felipe Rodriguez Espitia
#python-deteccion_figuras
#Descripcion:Programa que detecta el contorno de la figura y escribe que figura es dependiendo de sus lados.


import cv2
import numpy as np
#cargar imagen
#image = cv2.imread('figurasColores2.png')
#image = cv2.imread('tangram.png')
#image = cv2.imread('circulo.png')
image = cv2.imread('rectang.png')
#image = cv2.imread('rectangulo.png')
#image = cv2.imread('triangulo.png')
maxIntensity = 255.0 # depends on dtype of image data
x = np.arange(maxIntensity)

# Parametros para la manipulacion de imagenes
phi = 1
theta = 1
gr = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
gray = (maxIntensity/phi)*(gr/(maxIntensity/theta))**4
gray = np.array(gray,dtype=np.uint8)
cv2.imshow("Gray1",gr)
cv2.imshow("Gray",gray)
#canny = cv2.Canny(gray, 20, 60)
#Filtros que ayudan a la deteccion de bordes propios de las librerias opencv
canny = cv2.Canny(gray, 60, 180)
#canny = cv2.dilate(canny, None, iterations=1)
#canny = cv2.erode(canny, None, iterations=1)
cv2.imshow("Canny",canny)
#_, th = cv2.threshold(gray, 10, 255, cv2.THRESH_BINARY)
#_,cnts,_ = cv2.findContours(canny, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)# OpenCV 3
cnts,_ = cv2.findContours(canny, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)# OpenCV 4
#cv2.drawContours(image, cnts, -1, (0,255,0), 2)

for c in cnts:
	#Inicializacion del angulo para la deteccion
	epsilon = 0.01*cv2.arcLength(c,True)
	approx = cv2.approxPolyDP(c,epsilon,True)
	#print(len(approx))
	x,y,w,h = cv2.boundingRect(approx)

	if len(approx)==3:
		cv2.putText(image,'Triangulo', (x,y-5),1,1,(0,255,0),1)

	if len(approx)==4:
		aspect_ratio = float(w)/h
		#escribir el nombre de acuerdo al numero de lados y el angulo entre ellos
		print('aspect_ratio= ', aspect_ratio)
		if aspect_ratio == 1:
			cv2.putText(image,'Cuadrado', (x,y-5),1,1,(0,255,0),1)
		else:
			cv2.putText(image,'Rectangulo', (x,y-5),1,1,(0,255,0),1)

	if len(approx)==5:
		cv2.putText(image,'Pentagono', (x,y-5),1,1,(0,255,0),1)

	if len(approx)==6:
		cv2.putText(image,'Hexagono', (x,y-5),1,1,(0,255,0),1)

	if len(approx)>10:
		cv2.putText(image,'Circulo', (x,y-5),1,1,(0,255,0),1)

	cv2.drawContours(image, [approx], 0, (0,255,0),2)
	cv2.imshow('image',image)
	cv2.waitKey(0)
