#Realizado por: Luis Felipe Rodriguez Espitia
#Fecha: 08/12/2020

# importar librería OpenCV
import cv2 as cv
# Funciones de Sistema Operativo
import os
import numpy as np

initSequence=1 # inicio secuencia
numSequences= 100 # fin secuencia
cont_frame = 0 # contador auxiliar para implementar

#Directorio de lectura
path_RGB = "..//..//Datasets//17flowers"

# Directorios de escritura
#path_R = "../DATASETS/17flowers/R"
#path_G = "../DATASETS/17flowers/G"
#path_B = "../DATASETS/17flowers/B"
path_GRAY = "./Output-Gray"
path_Binary = "./Output-Binary"
path_Edges = "./Output-Edges"
path_Segmentation = "./Output-Segmentation"
path_Kmeans = "./Output-Kmeans"


totalImages = int(len(os.listdir(path_RGB))) # Contabiliza número de archivos
# Bucle recorre desde el inicio a fin en un rango
cont_frame = 0 # contador auxiliar para implementar
for ns in range(initSequence,numSequences+1):
    cont_frame = cont_frame + 1
    dirimages = path_RGB + '/image_' + str(ns).zfill(4) + '.jpg'
    img = cv.imread(dirimages)
    img2 = cv.imread(dirimages)
    # Transformar a ESCALA DE GRISES la imagen en color
    gray = cv.cvtColor(img2, cv.COLOR_BGR2GRAY)
    #binariza la imagen
    _, bin = cv.threshold(gray, 100, 255, cv.THRESH_BINARY)
    #filtro canny
    cany = cv.Canny(bin, 100, 200)

    #implementacion algoritmo watershet

    ret, thresh = cv.threshold(gray, 0, 255, cv.THRESH_BINARY_INV + cv.THRESH_OTSU)

    # Eliminación del ruido
    kernel = np.ones((3, 3), np.uint8)
    opening = cv.morphologyEx(thresh, cv.MORPH_OPEN, kernel, iterations=2)

    # Encuentra el área del fondo
    sure_bg = cv.dilate(opening, kernel, iterations=3)

    # Encuentra el área del primer
    dist_transform = cv.distanceTransform(opening, cv.DIST_L2, 5)
    ret, sure_fg = cv.threshold(dist_transform, 0.7 * dist_transform.max(), 255, 0)

    # Encuentra la región desconocida (bordes)
    sure_fg = np.uint8(sure_fg)
    unknown = cv.subtract(sure_bg, sure_fg)
    # Etiquetado
    ret, markers = cv.connectedComponents(sure_fg)

    # Adiciona 1 a todas las etiquetas para asegurra que el fondo sea 1 en lugar de cero
    markers = markers + 1

    # Ahora se marca la región desconocida con ceros
    markers[unknown == 255] = 0
    markers = cv.watershed(img, markers)
    img[markers == -1] = [255, 0, 0]

    ####KMEANS
    img2 = img.reshape((-1, 3))
    img2 = np.float32(img2)
    criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    # CLUSTERS
    k = 4
    attempts = 10
    ret1, label, center = cv.kmeans(img2, k, None, criteria, 10, cv.KMEANS_PP_CENTERS)
    center = np.uint8(center)
    res = center[label.flatten()]
    res2 = res.reshape((img.shape))


    cv.imwrite(path_GRAY + "/Grayimage_" + str(ns).zfill(4) + ".jpg", gray)
    cv.imwrite(path_Binary + "/Binaryimage_" + str(ns).zfill(4) + ".jpg", bin)
    cv.imwrite(path_Edges + "/Canyimage_" + str(ns).zfill(4) + ".jpg", cany)
    cv.imwrite(path_Segmentation + "/Segimage_" + str(ns).zfill(4) + ".jpg", img)
    cv.imwrite(path_Kmeans + "/Kmeans_" + str(ns).zfill(4) + ".jpg", res2)


