#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan  5 12:28:23 2019

@author: felipe
"""

import cv2 as cv
import numpy as np
#cap = cv.VideoCapture("v_shooting_16_04.mpg")
cap = cv.VideoCapture("Hooligans_violence__ACHTUNG.avi")
length = int(cap.get(cv.CAP_PROP_FRAME_COUNT)) 
print("Longutid: ",length) 
width = int(cap.get(cv.CAP_PROP_FRAME_WIDTH))
print("width: ",width)
height = int(cap.get(cv.CAP_PROP_FRAME_HEIGHT)) 
print("height: ",height)
fps = cap.get(cv.CAP_PROP_FPS) 
print("fps: ",fps)
codec = cap.get(cv.CAP_PROP_FOURCC)
print("codec: ",codec)
formato = cap.get(cv.CAP_PROP_FORMAT)
print("format: ",format)
ret, frame1 = cap.read()
prvs = cv.cvtColor(frame1,cv.COLOR_BGR2GRAY)
hsv = np.zeros_like(frame1)
hsv[...,1] = 255
print("shape: ",hsv.shape)
cont = 0
while(1):
    ret, frame2 = cap.read()
    next = cv.cvtColor(frame2,cv.COLOR_BGR2GRAY)
    flow = cv.calcOpticalFlowFarneback(prvs,next, None, 0.5, 3, 15, 3, 5, 1.2, 0)
    #print("flow-shape: ",flow.shape)
    
    #Lt = -u;
    Lt = -1*flow[...,0]
    # Si cada uno de los valores de la matriz u(horizontal) de la matriz (x) son >0 reemplazar por cero
    u = flow[...,0] 
#    for i in range(0, len(u)):
#        print("coordenadas: ",i)
    for i in range(len(u)):
        for j in range(len(u[i])):
            if u[i][j] > 0:
                Lt[i][j] = 0
            #print(u[i][j], end=' ')
    #print()
    Lt2 = cv.normalize(Lt,None,0,255,cv.NORM_MINMAX)
    #tempLt=cv.cvtColor(Lt2,cv.COLOR_HSV2BGR)
    cv.imshow('frameLT',Lt2)
    
    u2 = cv.normalize(u,None,0,255,cv.NORM_MINMAX)
    cv.imshow('frame-u2',u2)
#    k = cv.waitKey(30) & 0xff
    #Lt(u>0)=0; % Para dejar solo los vectores que van a la izquierda
    #print("flow: ",flow[0,1])
    #print("flow: ",flow[1])
    # convert from cartesian to polar
    mag, ang = cv.cartToPolar(flow[...,0], flow[...,1])
    #print("mag: ",mag, "ang: ",ang)
    # hue corresponds to direction
    hsv[...,0] = ang*180/np.pi/2
    #print("hsv[...,0]: ",hsv[...,0])
    # value corresponds to magnitude
    hsv[...,2] = cv.normalize(mag,None,0,255,cv.NORM_MINMAX)
    #print("hsv[...,2]: ",hsv[...,2])
    #print("hsv: ",hsv)
    #print(hsv)

    
    #print("MATRIZ hsv : "," # ",cont," ndim: ",hsv.ndim, " shape: ",hsv.shape)
    # convert HSV to int32's
    bgr = cv.cvtColor(hsv,cv.COLOR_HSV2BGR)
    cv.imshow('frame2',bgr)
    k = cv.waitKey(30) & 0xff
    if k == 27:
        break
    elif k == ord('s'):
        cv.imwrite('opticalfb.png',frame2)
        cv.imwrite('opticalhsv.png',bgr)
    prvs = next
cap.release()
cv.destroyAllWindows()
