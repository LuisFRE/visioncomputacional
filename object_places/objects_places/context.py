#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  5 18:12:04 2018

@author: felipe
"""

class context:

    def __init__(self,id_label, label, prob ):
        self.id_label = id_label
        self.label = label
        self.prob = prob
        
        


    def __str__(self):
        return "%s - %s - %f" % (self.id_label, self.label, self.prob)
    
